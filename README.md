# Streaming replication cluster for pgsql + pgpool2

[This project](https://bitbucket.org/doseme/postgres-docker-cluster) includes dockerfiles for building a postgresql cluster and example of [docker-compose](./docker-compose.yml) file to start this cluster. It is based on [https://github.com/paunin/postgres-docker-cluster](https://github.com/paunin/postgres-docker-cluster).

It varies from the version by not adding users to pg_hba.conf, and not exposing ports automatically. It has been designed to be deployed into an environment where a load balancer or proxy will handle this for you.

Two docker images can be produced:
* Postgresql server image which can start in master or slave mode: https://hub.docker.com/r/dosemeau/pgcluster-postgresql/
* Pgpool service with flexible configurations: https://hub.docker.com/r/dosemeau/pgcluster-pgpool/

## Schema of cluster

```
pgmaster (primary node1)  --|
|- pgslave1 (node2)       --|
|  |- pgslave2 (node3)    --|----pgpool (master_slave_mode stream)
|- pgslave3 (node4)       --|
   |- pgslave4 (node5)    --|
```

Each postgres node (pgmaster, pgslaveX) is managed by repmgr/repmgrd. It allows to use automatic failover. Depending on the environment settings, they will either stream off pgmaster or another slave.

To start a cluster, run it as a normal docker-compose application `docker-compose up`

Please check comments for each ENV variable in [docker-compose](./docker-compose.yml) file to understand the related parameter of cluster's node

## Useful commands

* Get map of current cluster(in any node): `gosu postgres repmgr cluster show`

## Scenarios

### Changing primary/master server on crash

Kill master container and in a few moments cluster will define new master node,
and Pgpool will try to detect new primary node for write access automatically.

## Known problems

* Killing of node in the middle (e.g. pgslave1) will cause nodes that stream off it to die as well. (https://groups.google.com/forum/?hl=fil#!topic/repmgr/lPAYlawhL0o)

## FAQ

* [How to promote master, after failover on postgresql with docker](http://stackoverflow.com/questions/37710868/how-to-promote-master-after-failover-on-postgresql-with-docker)

## Documentation and manuals

* Original project: https://github.com/paunin/postgres-docker-cluster
* Streaming replication in postgres: https://wiki.postgresql.org/wiki/Streaming_Replication
* Repmgr: https://github.com/2ndQuadrant/repmgr
* Pgpool2: http://www.pgpool.net/docs/latest/pgpool-en.html
* Kubernetes: http://kubernetes.io/
